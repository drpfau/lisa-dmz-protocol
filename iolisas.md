# iolisas

Socket-Client for connecting to a DMZ process

## General format (frame)

prefix[3]  
ALV - Alive-Telegramm
LIS - Message 

messageID[GUID 8-4-4-4-12 Format]

end[1]  
0x04 (EOT)

### Alive-Telegram
Alive-Telegramm[prefix+ending]  
prefix[3]  
ALV  

You have to send an Alive-Telegram at least every 10 seconds.

### Message
Message-Telegram[prefix+format+messageID+message+ending]  
prefix[3]  
LIS  
format[BYTE]  
0x01 - LISA legacy XML Event fo_xmleventmark  
0x02 - LISA legacy XML State fo_xmlstate  
0x10 - LISA (new JSON) (Meldungen mit ID-Nummer)  
0x11 - LISA Enterprise (new JSON) (Meldungen werden auf festgelegtem Objekt ausgelöst und bringen alle Informationen mit)  
0x20 - Saferworx  
0x21 - grafana webhook

## Acknoledgement (low level)
### Message
Acknowledgement telegram[prefix+messageID+ending]  
prefix[3]  
ACK - message gets processed  
NAK - can not be processed
### Alive-Telegramm
Acknowledgement telegram[ALV+prefix+ending]  
ACKALV
#### Alive-Telegramm Saferworx
ALVACK

# DMZ-Process

Darstellung des DMZ-Prozesses zum Empfang- und Weiterleitung von Meldungen an LISA

## Übersicht
```
                                   +-------------------------+                                                         
                                   |  DMZ-Process            |                                                         
                                   |                         |                                                         
                                   |                         |                                                         
                                   |                         |                                                         
                                   +-------------+           |                                                         
 +--------------------+      /-----|Interface    |           |                                              
 |Client              |------      |public       |           |                                                         
 |                    |          -/+-------------+           |                                                         
 |                    |         /  |           +-------------+                                                         
 +--------------------+       -/   |           |Socket-Server|                                                         
                             /     |           |LISA         |-\                                                        
                           -/      +-----------+-------------+  --\            +--------------------------+            
                         -/                                        ---\        |LISA-IO                   |            
+--------------------+  /                                              --\     |                          |            
|Client              |-/                                                  ---\ |-------------+            |            
|                    /                                                        -|Socket-Client|            |            
|                    |                                                         |DMZ          |            |            
+--------------------+                                                         |-------------+            |            
                                                                               |                          |            
                                                                               |                          |            
                                                                               |                          |            
                                                                               |                          |            
                                                                               |                          |            
                                                                               +--------------------------+            
```                                                                                                                       

Der DMZ-Prozess stellt mindestens zwei Interfaces bereit. Ein Interface auf der öffentlichen Seite der DMZ um die Meldungen der Endgeräte entgegen zu nehmen; einen Socketserver auf der inneren Seite der DMZ.  
LISA baut als Client zu diesem Prozess eine Verbindung auf. Über diese Verbindung nimmt LISA Meldungen im in `lisas-Protokoll.md` beschriebenen Format entgegen, quittiert diese gegenüber dem Empfangsprozess und überwacht die "Alive-Meldungen" des Empfangsprozesses.
      
## Meldungsweitergabe
Der DMZ-Prozess nimmt die Meldungen entgegen und gibt sie an LISA weiter. Wir empfehlen, dass der Prozess die Meldungen bis mindestens zur Quittierung durch LISA aufbewahrt für den Fall, dass LISA nicht verfügbar ist. 


# LISA JSON Formate

## LISA JSON
JSON-Meldungsschnittstelle  
`fo_lisajson`

### Ereignismeldungen
```json
{
    "TYPE": "EVENT",
    "MID": "b9682433-79bc-4b9b-a333-e149e6d071f2", //Message-ID GUID
    "CREATED": "2018-03-01T10:27:45.000Z", //ISO-Timestamp
    "ID": "4711",
    "EVENT": "BURGLARY", 
    "TEXT": "Einbruch 3. OG",
    "URL": "http://videosystem.example.com/?video=sGhnZ6gG"
}
```
Default-Events: FIRE, BURGLARY, ARMING, DISARMING, PANIC, MEDICAL, BATTERY_LOW, TAMPER, POWER_FAILURE, WATER, ALARM, TECHNICAL_ERROR, MAINTENANCE_BEGIN, MAINTENANCE_END


### Zustandsmeldungen
```json
{
    "TYPE": "LINESTATE",
    "MID": "...", //Message-ID GUID
    "CREATED": "...", //ISO-Timestamp
    "ID": "...",
    "MODULE": "...",  //Default 0
    "LINE": "...",    // 0-255
    "STATE": "...", //0, 1 - Ruhe/Auslösung
    "TEXT": "...",
    "URL": "..."
}
```

# Noch nicht vorgesehen

* Rückkanal (bspw. für Callback, oder GPS-On)
* Übertragung Stammdaten (ggf. separate Schnittstelle)
* Geodaten bei Ereignismeldung
